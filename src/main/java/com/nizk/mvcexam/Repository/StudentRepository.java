package com.nizk.mvcexam.Repository;

import com.nizk.mvcexam.Entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {


    boolean existsByStudentId(String studentId);
    StudentEntity findAllByStudentIdAndAssignmentId(String studentId, Integer assignmentId);
    List<StudentEntity> findAllByStudentId(String studentId);
    boolean existsByStudentIdAndAssignmentId(String studentId, Integer assignmentId);
}
