package com.nizk.mvcexam.Repository;

import com.nizk.mvcexam.Entity.AssignmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentRepository extends JpaRepository<AssignmentEntity, Integer> {


    boolean existsByAssignmentName(String assignmentName);

    AssignmentEntity findAllByAssignmentName(String assignmentName);
}
