package com.nizk.mvcexam.Controller;

import com.nizk.mvcexam.DTO.AssignmentReqDTO;
import com.nizk.mvcexam.Service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AssignmentController {

    @Autowired
    AssignmentService assignmentService;

    @GetMapping("/create-assignment") //html
    public String showCreateAssignment(Model model) {

        AssignmentReqDTO assignmentReqDTO = new AssignmentReqDTO();
        model.addAttribute("assignmentReqDTO", assignmentReqDTO);

        return "createAssignment";

    }


    @PostMapping("/create-assignment/save")
    public String createAssignment(@ModelAttribute("assignmentReqDTO") AssignmentReqDTO assignmentReqDTO,
                                   RedirectAttributes redirectAttrs) {
        String result = assignmentService.createAssignment(assignmentReqDTO);
        redirectAttrs.addFlashAttribute("result", result);
        return "redirect:/create-assignment";
    }



}
