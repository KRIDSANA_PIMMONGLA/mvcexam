package com.nizk.mvcexam.Controller;

import com.nizk.mvcexam.DTO.StudentReqDTO;
import com.nizk.mvcexam.Entity.StudentEntity;
import com.nizk.mvcexam.Service.AssignmentService;
import com.nizk.mvcexam.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class StudentController {

    @Autowired
    AssignmentService assignmentService;

    @Autowired
    StudentService studentService;

    @GetMapping("/create-work") //html
    public String showCreateWork(Model model) {

        StudentReqDTO studentReqDTO = new StudentReqDTO();
        model.addAttribute("studentReqDTO", studentReqDTO);

        return "createStudent";

    }


    @PostMapping("/create-work/save")
    public String createWork(@ModelAttribute("studentReqDTO") StudentReqDTO studentReqDTO,
                             RedirectAttributes redirectAttrs) {
        String result = studentService.createWork(studentReqDTO);
        redirectAttrs.addFlashAttribute("result", result);
        return "redirect:/create-work";
    }

    @GetMapping("/work") //html
    public String showWorkPage(Model model) {

        StudentReqDTO studentReqDTO = new StudentReqDTO();
        model.addAttribute("studentReqDTO", studentReqDTO);

        return "showWork";

    }

    @PostMapping("/show-work")
    public String showWork(@ModelAttribute("studentReqDTO") StudentReqDTO studentReqDTO,
                             RedirectAttributes redirectAttrs) {
        List<StudentEntity> studentEntityList = studentService.getWork(studentReqDTO);
        redirectAttrs.addFlashAttribute("studentEntityList", studentEntityList);
        return "redirect:/work";
    }



}
