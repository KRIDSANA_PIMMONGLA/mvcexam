package com.nizk.mvcexam.Entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "student")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "student_id")
    private String studentId;

    @Column(name = "assignment_id")
    private Integer assignmentId;

    @Column(name = "work_detail")
    private String workDetail;

    @Column(name = "submit_work_date")
    private String submitWorkDate;


}
