package com.nizk.mvcexam.Entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "assignment")
public class AssignmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "assignment_name")
    private String assignmentName;

    @Column(name = "assignment_detail")
    private String assignmentDetail;

    @Column(name = "assignment_deadline")
    private String assignmentDeadline;

}
