package com.nizk.mvcexam.Service;

import com.nizk.mvcexam.DTO.StudentReqDTO;
import com.nizk.mvcexam.Entity.AssignmentEntity;
import com.nizk.mvcexam.Entity.StudentEntity;
import com.nizk.mvcexam.Repository.AssignmentRepository;
import com.nizk.mvcexam.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    AssignmentRepository assignmentRepository;
    @Autowired
    StudentRepository studentRepository;

    public List<StudentEntity> getWork(StudentReqDTO studentReqDTO) {

            boolean status = true;

        return studentRepository.findAllByStudentId(studentReqDTO.getStudentId());

    }


    public String createWork(StudentReqDTO studentReqDTO) {

        try {

            boolean status = true;
            DateTimeFormatter formatterMain = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String submitDate = LocalDateTime.now().format(formatterMain);

            if (assignmentRepository.existsByAssignmentName(studentReqDTO.getAssignmentName())) {
                AssignmentEntity assignmentEntity = assignmentRepository.findAllByAssignmentName(studentReqDTO.getAssignmentName());
                if (studentRepository.existsByStudentIdAndAssignmentId(studentReqDTO.getStudentId(), assignmentEntity.getId())) {
                    StudentEntity studentEntity = studentRepository.findAllByStudentIdAndAssignmentId(studentReqDTO.getStudentId(), assignmentEntity.getId());
                    studentEntity.setWorkDetail(studentReqDTO.getWorkDetail());
                    studentEntity.setSubmitWorkDate(submitDate);
                    studentRepository.saveAndFlush(studentEntity);

                } else {
                    status = false;
                }

            } else {
                status = false;
            }


            if (status) {
                return "alert-success";
            } else {
                return "alert-danger";
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "alert-danger";
        }


    }

}
