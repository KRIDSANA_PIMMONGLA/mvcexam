package com.nizk.mvcexam.Service;

import com.nizk.mvcexam.DTO.AssignmentReqDTO;
import com.nizk.mvcexam.Entity.AssignmentEntity;
import com.nizk.mvcexam.Entity.StudentEntity;
import com.nizk.mvcexam.Repository.AssignmentRepository;
import com.nizk.mvcexam.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class AssignmentService {

    @Autowired
    AssignmentRepository assignmentRepository;
    @Autowired
    StudentRepository studentRepository;

    public String createAssignment(AssignmentReqDTO assignmentReqDTO) {

        try {

            boolean status;
            AssignmentEntity assignmentEntity = new AssignmentEntity();
            assignmentEntity.setAssignmentName(assignmentReqDTO.getAssignmentName());
            assignmentEntity.setAssignmentDetail(assignmentReqDTO.getAssignmentDetail());
            assignmentEntity.setAssignmentDeadline(assignmentReqDTO.getAssignmentDeadline());

            if (!assignmentRepository.existsByAssignmentName(assignmentEntity.getAssignmentName())) {

                System.out.println(assignmentEntity.getAssignmentDeadline());
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDateTime deadLineDate = LocalDate.parse(assignmentEntity.getAssignmentDeadline(), formatter).atStartOfDay();
                DateTimeFormatter formatterMain = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String deadLine = deadLineDate.format(formatterMain);
                assignmentEntity.setAssignmentDeadline(deadLine);
                AssignmentEntity assignmentEntitySave = assignmentRepository.save(assignmentEntity);

                String[] studentId = assignmentReqDTO.getStudentId().split(",");
                for (String s : studentId) {
                    StudentEntity studentEntity = new StudentEntity();
                    studentEntity.setStudentId(s.trim());
                    studentEntity.setAssignmentId(assignmentEntitySave.getId());
                    studentRepository.save(studentEntity);
                }

                status = true;
            } else {
                status = false;
            }

            if(status) {
                return "alert-success";
            }else{
                return "alert-danger";
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "alert-danger";
        }


    }

}
