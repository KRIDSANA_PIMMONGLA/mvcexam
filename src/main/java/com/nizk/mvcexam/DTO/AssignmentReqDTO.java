package com.nizk.mvcexam.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignmentReqDTO {

    private String assignmentName;

    private String assignmentDetail;

    private String assignmentDeadline;

    private String studentId;

}
