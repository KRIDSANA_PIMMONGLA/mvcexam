package com.nizk.mvcexam.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentReqDTO {


    private String studentId;

    private String assignmentName;

    private String workDetail;

    private String submitWorkDate;

}
